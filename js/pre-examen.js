function buscarUsuario() {
    const userId = document.getElementById('userId').value;

    if (!userId) {
        alert('Por favor, ingrese un ID de usuario');
        return;
    }

    const apiUrl = `https://jsonplaceholder.typicode.com/users/${userId}`;

    axios.get(apiUrl)
        .then(response => {
            const userData = response.data;
            mostrarResultado(userData);
        })
        .catch(error => {
            console.error('Error al realizar la petición:', error);
            alert('No se pudo obtener la información del usuario. Por favor, verifique el ID.');
        });
}

function mostrarResultado(userData) {
    document.getElementById('name').value = userData.name;
    document.getElementById('username').value = userData.username;
    document.getElementById('email').value = userData.email;

    // Separar la dirección en partes y mostrarlas en campos de texto individuales
    document.getElementById('street').value = userData.address.street;
    document.getElementById('suite').value = userData.address.suite;
    document.getElementById('city').value = userData.address.city;
}
