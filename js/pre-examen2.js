function buscarPais() {
    const countryName = document.getElementById('countryName').value;

    if (!countryName) {
        alert('Por favor, ingrese el nombre del país');
        return;
    }

    const apiUrl = `https://restcountries.com/v3.1/name/${countryName}`;

    fetch(apiUrl)
        .then(response => {
            if (!response.ok) {
                throw new Error('No se encontró información para el país ingresado');
            }
            return response.json();
        })
        .then(data => {
            mostrarInformacion(data[0]);
        })
        .catch(error => {
            console.error('Error al realizar la petición:', error);
            alert(error.message);
        });
}

function mostrarInformacion(countryData) {
    document.getElementById('capital').textContent = countryData.capital || 'No disponible';

    // Obtener el código de idioma independientemente de la posición en la respuesta
    const language = Object.values(countryData.languages)[0] || 'No disponible';
    document.getElementById('language').textContent = language;
}

function limpiar() {
    document.getElementById('countryName').value = '';
    document.getElementById('capital').textContent = '';
    document.getElementById('language').textContent = '';
}
