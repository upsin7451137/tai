const llamadoFetch = () => {
    const url ="https://jsonplaceholder.typicode.com/todos";
    fetch(url)
        .then((respuesta) => respuesta.json())  
        .then(data => mostrarTodos(data))
        .catch((reject) => {
            console.log("Surgió un error: " + reject);
        });
}

//usando await

const llamadoAwait = async () => {
    try {
        const url = "https://jsonplaceholder.typicode.com/todos";
        const respuesta = await fetch(url);
        const data = await respuesta.json();
        mostrarTodos(data);
    } catch (error) {
        console.log("Surgió un error " + error);
    }
}

const mostrarTodos = (data) => {
    console.log(data);

    const res = document.getElementById('respuesta');
    res.innerHTML = "";
    for (let item of data) {
        res.innerHTML += item.userId + "," + item.id + "," + item.title + "" + item.completed + "<br>";
    }
}

// Codificar los botones
document.getElementById("btnCargarP").addEventListener('click', function () {
    llamadoFetch();
});

document.getElementById("btnCargarA").addEventListener('click', function () {
    llamadoAwait();
});

document.getElementById("btnLimpiar").addEventListener('click', function () {
    let res = document.getElementById("respuesta");
    res.innerHTML = "";
});
