function hacerPeticionPorId(userId) {
    const http = new XMLHttpRequest();
    const url = `https://jsonplaceholder.typicode.com/users/${userId}`;

    http.onreadystatechange = function() {
        if (this.status == 200 && this.readyState == 4) {
            let res = document.getElementById("lista");
            res.innerHTML = ""; // Limpiar la tabla antes de mostrar los nuevos resultados
            const datos = JSON.parse(this.responseText);

            res.innerHTML += '<tr> <td class="columna1">' + datos.id + '</td>'
                + '<td class="columna2">' + datos.name + '</td>'
                + '<td class="columna3">' + datos.username + '</td>'
                + '<td class="columna4">' + datos.email + '</td>'
                + '<td class="columna5">' + datos.phone + '</td>'
                + '<td class="columna6">' + datos.website + '</td> </tr>';

            res.innerHTML += "</tbody>";
        }
    };

    http.open('GET', url, true);
    http.send();
}

document.getElementById("btnMostrarPorId").addEventListener("click", function() {
    let userId = document.getElementById("userId").value;
    if (userId !== "") {
        alert("Mostrando datos por ID...");
        hacerPeticionPorId(userId);
    } else {
        alert("Por favor, ingrese un ID de usuario.");
    }
});

document.getElementById("btnLimpiar").addEventListener("click", function() {
    let res = document.getElementById("lista");
    res.innerHTML = "";
});
